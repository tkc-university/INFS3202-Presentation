
<!-- -- .slide: data-background-image="media/uq.png" data-state="title-slide" -->
# Assessmentor
#### Thomas Cranny & Alek Thompson

---


## What?

- Website & mobile application
- Visualise and track progress through a semester

---

## Why?
- Assessment load and progress isn't clear
- Visualise current progress and how much is left

Note:
- It can also helps to by making a game that can be scored in

--

<!-- -- .slide: data-state="table-slide" -->
![Assessment Table](media/assessment_table.png) <!-- -- .element: class="big" -->

Note: 
- Assessment load isn't clear
- Wanted to visualise current progress and how much is left
<hr>
- We're provided the ECP, though its dates can be ambiguous
- Takes effort and usually a spreadsheet to actually figure out your grade

<hr>
- We're provided the ECP, though its dates can be ambiguous.
- Takes effort and usually a spreadsheet to actually figure out your grade.

---


## How?
Two parts: **Front** and **Back** ends

(and Deployment) <!-- .element: class="fragment" -->

--

### Back End
- RESTful JSON API
- Uses **Python**, with **Flask** and **SQLAlchemy**
- **PostgreSQL** for the database

![Backend End Logos](media/backend.png) 

--

### Front End
- HTML rendering with **React**
- State management with **Redux**
- **Cordova** for Android and iOS versions

![Front End Logos](media/frontend.png)

--

### Deployment
- **Ansible** deploys Front and Back ends
- **Supervisord** manages and monitors workers
- **Gunicorn** for Python workers
- **Nginix** terminates SSL and serves static resources

![Deployment Logos](media/deployment.png)

---


## Advanced Features
### Back End


--


## Semantic API

- API is RESTful <!-- .element: class="fragment" -->
- Follows the HATEOAS architecture <!-- .element: class="fragment" -->

```python
requests.get('infs3202.tkc.tech/api/semester/1').json()
```
<!-- .element: class="fragment" -->

```json
{
   "name": "Semester 1, 2017",
   "id": 1, 
   "start": "Semester 1, 2017",
   "end": "2017-06-23T00:00:00+00:00",
   "_links": {
      "courses": [
        "/api/course/1", 
        "/api/course/2", 
        "/api/course/3"
      ], 
      "self": "/api/semester/1", 
      "user": "/api/user/1"
   }
}
```
<!-- .element: class="fragment" -->

Note:
- Hypermedia As The Engine Of Application State

--

## Security

- All API requests must use a cryptographically signed tokens <!-- .element: class="fragment" -->
- Tokens have a timeout, to mitigate token theft <!-- .element: class="fragment" -->
- No server-side state is required <!-- .element: class="fragment" -->

```python
details = ('michael.bluth@uq.net.au', 'hunter2')
requests.get(
    'infs3202.tkc.tech/api/auth/token', auth=details
).json()
```
<!-- .element: class="fragment" -->

```json
{
  "auth_token": "eyJhbGci[...]2DY3t6ac", 
  "id": 18, 
  "name": "Michael Bluth"
}
```
<!-- .element: class="fragment" -->


---


## Advanced Features
### Front End


--


## Single Page
- API only <!-- .element: class="fragment" -->
- HTML/CSS/JS therefore cached <!-- .element: class="fragment" -->
- Very quick response times <!-- .element: class="fragment" -->


--


## Interactive charts

- Interactive and explorable <!-- .element: class="fragment" -->
- Intuitive <!-- .element: class="fragment" -->
- Detailed <!-- .element: class="fragment" -->

Note:
- Provide a level of interaction that the "My Grades" section on Blackboard doesn't.
- Charts are better as the human brain is hugely optimised for vision, reading is slow and hard to retain in memory.


--


## Data Driven
- All content is customised user content <!-- .element: class="fragment" -->
- Application supports any type of assessment <!-- .element: class="fragment" -->


---


# <a href="https://infs3202.tkc.tech" target="_blank">Demo</a>
